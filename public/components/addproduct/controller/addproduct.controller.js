(function () {
	angular
		.module('Shop')
		.controller('AddProductController', ['$http', '$state', AddProductController]);

	function AddProductController ($http, $state) {
		var self = this;
	
		self.id = 'test',
		self.productName = '',
		self.price = '',
		self.propreties = '',
		self.color = ''

		self.addProduct = function addProduct () {
			
			$http.post('/shop', {
					id: self.id,
					productName: self.productName,
					price: self.price,
					propreties: self.propreties,
					color: self.color

			}).then( function success (res) {
				console.log('add success');
				$state.go('shop.products');
			}, function error (err) {
				console.log(err);
			});
		};
	};
}());