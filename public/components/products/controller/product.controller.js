(function () {
	'use strict';

	angular
		.module('Shop')
		.controller('ProductController', ['$http','$state', ProductController]);

	function ProductController ($http, $state) {
		var self = this;

		self.getProducts = function getProducts () {
			$http.get('/shop').then(function success (res) {
				self.products = res.data;
				console.log('success contact list');
			}, function error (err) {
				console.log(err);
			});
		};
		self.getProducts();

		self.getProduct = function getProduct (id) {
			$state.go('shop.product.detail', {id: id});
		};

		self.editProduct = function editProduct (id) {
			$state.go('shop.product.edit', {id: id}, {reload: true});
		};

		self.deleteProduct = function deleteProduct (id, index) {
			$http.delete('/shop/' + id).then ( function success (res) {
				console.log('delete success');
				$state.go('shop.products', {id:id}, {reload: true});

			}, function error (err) {
				console.log(err);
			});
		};

	}

}());

















































// (function () {
// 	'use strict';

// 	angular
// 		.module('Shop')
// 		.controller('ProductController', ['$state', '$http', '$stateParams', 'Api', ProductController]);

// 	function ProductController ($state, $http, $stateParams, Api) {
// 		var self = this;

// 		// Api.get('/get')
// 		// 	.then(function success (res) {
// 		// 		console.log(res);
// 		// 	}, function error (err) {
// 		// 		console.log('callback error', err);
// 		// 	});

// 		console.log('stateparams: ', $stateParams, $state.params);

// 		self.product = {};		

// 		self.getById = function getById () {
// 			$http.get('/shop/' + $state.params.id).then( function success (res) {
// 				self.product = res.data[0];
// 			},function error (err) {
// 				console.log(err);
// 			});
// 		};

// 		if ($state.current.name !== 'shop.product.newproduct') {
// 			console.log('edit mode');
// 			self.getById();
// 		}

// 		self.editProduct = function editProduct () {
// 			$http.put('/shop/' + $state.params.id, self.product).then( function success (res) {
				
// 			$state.go('shop.products');
// 			}, function error (err) {
// 				console.log(err);
// 			});
// 		};

// 		self.addProduct = function addProduct () {
// 			console.log('product', self.product);
// 			$http.post('/shop', self.product).then( function success (res) {
// 				self.array = res.data;
// 				console.log('Success added Contact', self.array);
// 				$state.go('shop.products');
					
// 			}, function error (err) {
// 				console.log(err);
// 			});
// 		};

// 		self.Update = function Update () {
// 			console.log($state.current.name);
// 			if( $state.current.name === 'shop.product.edit') {

// 				self.editProduct();

// 			} else {
// 				console.log($state.current.name);
// 				self.addProduct();

// 			}
// 		}
// 	}

// }());