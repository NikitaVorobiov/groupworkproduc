(function () {
	angular  
		.module('Shop')
		.controller('ProductListController', ['$http','$state', ProductListController]);

	function ProductListController ($http, $state) {
		var self = this;

		self.getAllProducts = function getAllProducts () {
			
			$http.get('/shop').then( function success (res) {

				self.array = res.data;

			
			}, function error (err) {
				console.log(err);
			});
		};

		self.getAllProducts();

		self.showDetails = function showDetails (id) {
			$state.go('shop.products.product', {id: id});
		};
	};
}()); (function () {
	'use strict';

	angular
		.module('Shop')
		.controller('ProductListController', ['$http', '$stateParams', '$state', ProductListController]);

	function ProductListController ($http, $stateParams, $state) {
		var self = this;

		self.id = '',
		self.productName = '',
		self.price = '',
		self.propreties = '',
		self.color = ''


		self.getById = function getById () {

			$http.get('/shop/' + $state.params.id).then( function success (res) {
				self.Shop = res.data;
				console.log('contact Info');

			},function error (err) {
				console.log(err);
			});

		};

		self.getById();


		self.editProduct = function editProduct () {
			$http.put('/app/' + $state.params.id, {
					id: self.id,
					productName: self.productName,
					price: self.price,
					propreties: self.propreties,
					color: self.color

			}).then( function success (res) {
				console.log('product was edit');
				$state.go('shop.products');

			}, function error (err) {
				console.log(err);
			});
		};

	}

}());