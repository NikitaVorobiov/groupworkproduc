(function () {
	'use strict';

	angular
		.module('Shop')
		.config(routes);

	function routes ($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state({
				name: 'shop',
				url: '/shop',
				controller: 'ShopController',
				controllerAs: 'shop',
				templateUrl: 'components/app/template/template.html'

		  }).state({
		  		name: 'shop.products',
		  		url: '/products',
		  		controller: 'ProductListController',
		  		controllerAs: 'productList',
		  		templateUrl: 'components/products/template/productlist.template.html'

		 }).state({
				name: 'shop.products.product',
				url: '/product/:id',
				controller: 'ProductController',
				controllerAs: 'product',
				templateUrl: 'components/products/template/product.template.html'

		}).state({
				name: 'shop.products.product.edit',
				url: '/:id',
				templateUrl: 'components/products/template/formproduct.template.html'

		}).state({
				name: 'shop.products.product.delete',
				url: '/:id',
				templateUrl: 'components/products/template/formcontact.template.html'

		}).state({
				name:'shop.addnewproduct',
				url:'/addProduct',
				controller:'AddProductController',
				controllerAs: 'add',
				templateUrl: 'components/addproduct/template/addproduct.template.html'
		})


		$urlRouterProvider.otherwise('shop');
	};

}());