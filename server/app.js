/**
 * Created by hanni on 11/14/16.
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.json());

require('./routes').routes(app);

var server = app.listen('3333', function () {
    console.log('Server port: ' + server.address().port);
});