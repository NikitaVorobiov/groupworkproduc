/**
 * Created by hanni on 11/14/16.
 */

var uuid = require('node-uuid');

//array of data
var products = [
    {
        id: '1',
        productName: 'Mouse',
        price: 86,
        propreties:'Norm tak Zaebis.',
        color: 'Red'
    },
    {
        id: '2',
        productName: 'Phone',
        price: 1488,
        propreties:'Telefon admina prodati srocno nado.',
        color: 'Black'
    },
    {
        id: '3',
        productName: 'TV',
        price: 12,
        propreties:'Babkin telik eshe rabotaet.',
        color: 'Hren znaet'
    }
];

//GET all products
exports.getAllProducts = function getAllProducts(req, res) {
    res.send(products);
};

//GET only one product
exports.getProductById = function getProductById(req, res) {
    if (!req.params.id){
        res.status(401).send('Id of product is mandatory!');
        return null;
    }

    //We searching product with the same id
    var index = searchById(req.params.id);

    //if we have the same product we will send him else we will throw error
    if(index || index == 0) {
        res.send(products[index]);
    } else {
        res.send.status(404).send('We dont have: ' + req.params.id);
    }
};

//POST api
exports.addNewProduct = function addNewProduct(req, res) {
    if (!req.body.productName){
        res.status(401).send('Name of product is mandatory!');
        return null;
    }
    if (!req.body.price){
        res.status(401).send('Price of product is mandatory!');
        return null;
    }
    if (!req.body.propreties){
        res.status(401).send('Propreties of product is mandatory!');
        return null;
    }
    if (!req.body.color){
        res.status(401).send('Color of product is mandatory!');
        return null;
    }

    var obj = {
        id: uuid.v4(),
        productName: req.body.productName,
        price: req.body.price,
        propreties:req.body.propreties,
        color: req.body.color
    };

    //Adding new object in array
    products.push(obj);

    res.send('Added');
};

//update API input updated productName,price,propreties,color and id
//output message 'Updated'
exports.updateProduct = function updateProduct(req, res) {
    if (!req.params.id){
        res.status(401).send('Id of product is mandatory!');
        return null;
    }
    if (!req.body.productName){
        res.status(401).send('Name of product is mandatory!');
        return null;
    }
    if (!req.body.price){
        res.status(401).send('Price of product is mandatory!');
        return null;
    }
    if (!req.body.propreties){
        res.status(401).send('Propreties of product is mandatory!');
        return null;
    }
    if (!req.body.color){
        res.status(401).send('Color of product is mandatory!');
        return null;
    }

    //index element in array of objects
    var index = searchById(req.params.id);

    //if we have the same product we will update him else we will throw error
    if (index || index == 0){
        //update data
        products[index].productName = req.body.productName;
        products[index].price = req.body.price;
        products[index].propreties = req.body.propreties;
        products[index].color = req.body.color;

        res.send('Updated');

    } else {
        res.status(404).send('We dont have: ' + req.params.id);
    }
};

//We delete element by id
exports.deleteProduct = function deleteProduct(req, res) {
    if (!req.params.id){
        res.status(401).send('Id of product is mandatory!');
        return null;
    }

    //We searching product with the same id
    var index = searchById(req.params.id);

    //if we have the same product we will delete him else we will throw error
    if (index || index == 0) {
        products.splice(index, 1);
        res.send('Deleted')

    } else {
        res.status(404).send('We dont have: ' + req.params.id);
    }
};


//Find index of the element by him id if array has this id function will return index else function return null
function searchById(productId) {
    var productIndex = '';

    products.forEach( function (value, index) {
        if (value.id == productId){
            productIndex = index;
        }
    });

    if (productIndex.length < 1) {
        return false;
    } else {
        return productIndex;
    }
}
