/**
 * Created by hanni on 11/14/16.
 */

exports.routes = function routes (app) {

    var products =require('./components/product');

    app.get('/products', products.getAllProducts);
    app.get('/products/:id', products.getProductById);
    app.post('/products', products.addNewProduct);
    app.put('/products/:id', products.updateProduct);
    app.delete('/products/:id', products.deleteProduct);
};